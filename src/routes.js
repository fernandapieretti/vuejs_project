import Home from './components/Home/Home.vue';
import Detalhe from './components/detalhe/Detalhe.vue';

export const routes = [

    { path: '', name: 'home', component: Home },
    { path: '/detalhe/:id', name: 'detalhe', component: Detalhe, titulo: 'Cadastro' },

];